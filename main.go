package main

import (
	"bufio"
	"log"
	"os"
	"text/template"
	"strings"
)

type Item struct {
	Name string
	Path string
}

func load(fpath string) []Item {
	var items []Item

	f, _ := os.Open(fpath)
	defer f.Close()

	bs := bufio.NewScanner(f)
	for bs.Scan() {
		split := strings.Split(bs.Text(), ":::")
		items = append(items, Item{split[0], split[1]})
	}

	return items
}

func main() {
	b, err := os.ReadFile("template.html")
	if err != nil {
		log.Fatalf("os.ReadFile: %s\n", err.Error())
	}

	tpl := template.Must(template.New("ppd").Parse(string(b)))

	of, err := os.Create("index.html")
	if err != nil {
		log.Fatalf("os.Create: %s\n", err.Error())
	}
	defer of.Close()

	videos := load("data/video.txt")
	startVideo := videos[0]

	err = tpl.ExecuteTemplate(of, "ppd", map[string]interface{}{
		"StartVideoName": startVideo.Name,
		"StartVideoPath": startVideo.Path,
		"Videos": videos,
		"References": load("references.txt"),
	})
	if err != nil {
		log.Fatalf("tpl.ExecuteTemplate: %s\n", err.Error())
	}
}
